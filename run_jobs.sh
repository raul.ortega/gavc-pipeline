#!/bin/bash

# Función para aplicar un archivo YAML y esperar a que termine
apply_yaml() {
    local yaml_file="$1"
    local job_name=$(kubectl apply -f "$yaml_file" --dry-run=client -o json | jq -r '.items[] | select(.kind == "Job") | .metadata.name')
    
    if [[ -z "$job_name" ]]; then
        echo "Error: No se pudo obtener el nombre del job del archivo YAML $yaml_file."
        return 1
    fi

    local start_time
    local elapsed_time=0

    # Verificar si ya existe un job con el mismo nombre
    if kubectl get job "$job_name" >/dev/null 2>&1; then
        start_time=$(kubectl get job "$job_name" -o jsonpath='{.status.startTime}')
        start_time=$(date -d "$start_time" +%s)
        echo "Job $job_name was launched on $(date -d @"$start_time" +"%T %D"), it won't be launche again."
    else
        start_time=$(date +%s)
        echo "Launching $yaml_file..."
        kubectl apply -f "$yaml_file"
        echo -n "Waiting $yaml_file to complete... "
    fi

    #kubectl wait --for=condition=complete --timeout=-1s -f "$yaml_file" >/dev/null 2>&1 &
    kubectl wait --for=condition=complete --timeout=-1s job/${job_name} >/dev/null 2>&1 &

    # Wait for the background process to finish
    local pid=$!
    wait $pid >/dev/null 2>&1
    
    #while kill -0 $pid 2>/dev/null; do
        local current_time=$(date +%s)
        local elapsed_time=$((current_time - start_time))
        local days=$((elapsed_time / 86400))
        local hours=$(( (elapsed_time % 86400) / 3600 ))
        local minutes=$(( (elapsed_time % 3600) / 60 ))
        local seconds=$((elapsed_time % 60))
        if [ $days -gt 0 ]; then
            printf "\rElapsed time for $yaml_file: %dd %02d:%02d:%02d" $days $hours $minutes $seconds
        else
            printf "\rElapsed time for $yaml_file: %02d:%02d:%02d" $hours $minutes $seconds
        fi
        #sleep 10
    #done
    completion_time=$(kubectl get job ${job_name} -o=jsonpath='{.status.startTime}{"\t"}{.status.completionTime}{"\n"}' | awk -F '[TZ:-]' '{inicio=mktime($1 " " $2 " " $3 " " $4 " " $5 " " $6); fin=mktime($7 " " $8 " " $9 " " $10 " " $11 " " $12); diferencia=fin-inicio; printf "%d días, %d horas, %d minutos y %d segundos\n", diferencia/86400, (diferencia%86400)/3600, (diferencia%3600)/60, diferencia%60}')
    echo -e "\nJob ${job_name} has been completed in $completion_time"
}

# Verificar si se proporcionaron archivos YAML como argumentos
if [ $# -eq 0 ]; then
    echo "You must provide at least one YAML file as an argument."
    exit 1
fi

# Iterar sobre cada archivo YAML proporcionado y aplicarlo secuencialmente
for yaml_file in "$@"; do
    apply_yaml "$yaml_file"
done

echo "All jobs have been launched and completed."


