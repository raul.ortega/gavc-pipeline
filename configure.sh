#!/bin/bash

# general variables
filename_pattern='([A-Za-z0-9]+)-([0-9]+)_([0-9]+)_([A-Za-z]+)_R([0-9]+)\.fastp\.fastq\.gz' # the common pattern of all fastq files that will be processed 
individual_prefix="c_lc_zz"                                                                 # sample names will start with this prefix
output_file_samples="samples.csv"                                                           # name of the file to store the sample table				
output_bed_file_mapping="bed-mapping.csv"                                                   # name of the file to store the beds table

# Title message
echo -e "Please provide the following data:"

# declare parameters
#   default values should be provided as env variables:
#     USER_NAME_DEFAULT="user"
#     PARALLELISM_ALIGNMENT_DEFAULT="6"
#     PARALLELISM_MERGE_DEFAULT="6"
#     PARALLELISM_VARIANT_DEFAULT="20"
#     NFS_SERVER_DEFAULT="X.X.X.X"
#     NFS_PATH_DEFAULT="/your_nfs_path"
#     NODE_DEFAULT="node_name"

user_name=""
parallelism_alignment=""
parallelism_merge=""
parallelism_variant=""
nfs_server=""
nfs_path=""
node=""

if [ -z "$1" ]; then
    user_name="$USER_NAME_DEFAULT"
    parallelism_alignment="$PARALLELISM_ALIGNMENT_DEFAULT"
    parallelism_merge="$PARALLELISM_MERGE_DEFAULT"
    parallelism_variant="$PARALLELISM_VARIANT_DEFAULT"
    nfs_server="$NFS_SERVER_DEFAULT"
    nfs_path="$NFS_PATH_DEFAULT"
    node="$NODE_DEFAULT"
elif [ "$1" = "-i" ]; then
    # Ask user for user name (Linux username format only)
    while true; do
        read -rp "User name: (default: '$USER_NAME_DEFAULT'):" input_user_name
        # Check if input is not empty and is a valid Linux username
        if [[ -z "$input_user_name" ]]; then
            user_name="$USER_NAME_DEFAULT"
            break
        elif [[ "$input_user_name" =~ ^[a-z_][a-z0-9_-]{0,30}$ ]]; then
            user_name="$input_user_name"
            break
        else
            echo "Invalid input. Please enter a valid Linux username."
        fi
    done

    # Ask user for alignment parallelism (integer only)
    while true; do
        read -rp "Alignment parallelism (default: '$PARALLELISM_ALIGNMENT_DEFAULT'): " input_alignment
        # Check if input is not empty and is a valid integer
        if [[ -z "$input_alignment" ]]; then
            parallelism_alignment="$PARALLELISM_ALIGNMENT_DEFAULT"
            break
        elif [[ "$input_alignment" =~ ^[0-9]+$ ]]; then
            parallelism_alignment="$input_alignment"
            break
        else
            echo "Invalid input. Please enter an integer."
        fi
    done

    # Ask user for merge parallelism (integer only)
    while true; do
        read -rp "Merge parallelism (default: '$PARALLELISM_MERGE_DEFAULT'): " input_merge
        # Check if input is not empty and is a valid integer
        if [[ -z "$input_merge" ]]; then
            parallelism_merge="$PARALLELISM_MERGE_DEFAULT"
            break
        elif [[ "$input_merge" =~ ^[0-9]+$ ]]; then
            parallelism_merge="$input_merge"
            break
        else
            echo "Invalid input. Please enter an integer."
        fi
    done

    # Ask user for variant parallelism (integer only)
    while true; do
        read -rp "Variant parallelism (default: '$PARALLELISM_VARIANT_DEFAULT'): " input_variant
        # Check if input is not empty and is a valid integer
        if [[ -z "$input_variant" ]]; then
            parallelism_variant="$PARALLELISM_VARIANT_DEFAULT"
            break
        elif [[ "$input_variant" =~ ^[0-9]+$ ]]; then
            parallelism_variant="$input_variant"
            break
        else
            echo "Invalid input. Please enter an integer."
        fi
    done

    # Ask user for NFS server (IP address only)
    while true; do
        read -rp "NFS server (default: '$NFS_SERVER_DEFAULT'): " input_nfs_server
        # Check if input is not empty and is a valid IP address
        if [[ -z "$input_nfs_server" ]]; then
            nfs_server="$NFS_SERVER_DEFAULT"
            break
        elif [[ "$input_nfs_server" =~ ^([0-9]{1,3}\.){3}[0-9]{1,3}$ ]]; then
            nfs_server="$input_nfs_server"
            break
        else
            echo "Invalid input. Please enter a valid IP address."
        fi
    done

    # Ask user for NFS path (Linux path format only)
    while true; do
        read -rp "NFS path (default: '$NFS_PATH_DEFAULT'): " input_nfs_path
        # Check if input is not empty and is a valid Linux path
        if [[ -z "$input_nfs_path" ]]; then
            nfs_path="$NFS_PATH_DEFAULT"
            break
        elif [[ "$input_nfs_path" =~ ^/.* ]]; then
            nfs_path="$input_nfs_path"
            break
        else
            echo "Invalid input. Please enter a valid Linux path."
        fi
    done

    # Ask user for worker node (default: 'node')
    while true; do
        read -rp "Worker node (default: '$NODE_DEFAULT'): " input_node
        if [[ -z "$input_node" ]]; then
            node="$NODE_DEFAULT"
            break
        else
            node="$input_node"
            break
        fi
    done
fi

# Convert slashes to escaped slashes in the entered NFS path value
nfs_path="${nfs_path//\//\\/}"

# Show the user-entered values
echo -e "\n Jobs will be configured with this parameters:\n"
echo "  User name: $user_name"
echo "  Alignment parallelism: $parallelism_alignment"
echo "  Merge parallelism: $parallelism_merge"
echo "  Variant parallelism: $parallelism_variant"
echo "  NFS server: $nfs_server"
echo "  NFS path: $nfs_path"
echo -e "  Worker node: $node\n"

# Configure job prepare-genome
input_yaml="prepare-genome.yaml"
output_yaml="${user_name}_${input_yaml}"
sed "s/username/${user_name}/g" ${input_yaml} > ${output_yaml}
sed -i "s/PARALLELISM/1/g" ${output_yaml}
sed -i "s/COMPLETIONS/1/g" ${output_yaml}
sed -i "s/NFS_SERVER/${nfs_server}/g" ${output_yaml}
sed -i "s/NFS_PATH/${nfs_path}/g" ${output_yaml}
sed -i "s/NODE/${node}/g" ${output_yaml}

# Launch job prepare-genome and wait until it has finished
job_name=$(kubectl apply -f "${output_yaml}" --dry-run=client -o json | jq -r '.metadata.name')
    
echo "Job name: $job_name"

if [[ -z "$job_name" ]]; then
    echo "Error: Couldn't retrieve the job name from the YAML file $yaml_file."
    return 1
fi

elapsed_time=0

# Check if a job with the same name already exists
if kubectl get job "$job_name" >/dev/null 2>&1; then
  start_time=$(kubectl get job "$job_name" -o jsonpath='{.status.startTime}')
  start_time=$(date -d "$start_time" +%s)
  echo "The job $job_name is already running since $(date -d @"$start_time" +"%T %D"), it will not be launched again."
  echo "Waiting for $job_name to finish..."
else
  start_time=$(date +%s)
  echo "Launching ${output_yaml}..."
  kubectl apply -f "${output_yaml}"
  echo -n "Waiting for ${output_yaml} to finish..."
fi
    
kubectl wait --for=condition=complete --timeout=-1s -f "${output_yaml}" >/dev/null 2>&1 &

# Wait for the background process to finish
pid=$!
wait $pid >/dev/null 2>&1

#while kill -0 $pid 2>/dev/null; do
    current_time=$(date +%s)
    elapsed_time=$((current_time - start_time))
    days=$((elapsed_time / 86400))
    hours=$(( (elapsed_time % 86400) / 3600 ))
    minutes=$(( (elapsed_time % 3600) / 60 ))
    seconds=$((elapsed_time % 60))
    if [ $days -gt 0 ]; then
        printf "\rElapsed time for ${output_yaml}: %dd %02d:%02d:%02d" $days $hours $minutes $seconds
    else
        printf "\rElapsed time for ${output_yaml}: %02d:%02d:%02d" $hours $minutes $seconds
    fi
    #sleep 1
#done

# get list of fasq files
kubectl logs -n ${user_name}-namespace $(kubectl get pod -n ${user_name}-namespace | grep "prepare-genome" | awk '{ print $1 }') | sed -n '/ls -1/,/cat/ {/cat/d; p}' > fastq_files.txt

# get lsit of regions from genome.fa.fai 
kubectl logs -n ${user_name}-namespace $(kubectl get pod -n ${user_name}-namespace | grep "prepare-genome" | awk '{ print $1 }') | sed -n '/cat/,$p' > regions.txt

# Create samples.csv file
echo -e "\n\nCreating ${output_file_samples} file"
tail -n +2 fastq_files.txt | while IFS= read -r file; do
  # match pattern
  if [[ $file =~ $filename_pattern ]]; then
    # get variable values
    sample_prefix="${BASH_REMATCH[1]}"
    individual_id="${BASH_REMATCH[2]}"
    fastq_number="${BASH_REMATCH[3]}"
    sequence="${BASH_REMATCH[4]}"
    read_id="${BASH_REMATCH[5]}"
        
    # set columns value
    sample_name="${individual_prefix}_$(printf "%04d" $individual_id)"
    fastq_id="${sample_prefix}-${individual_id}_${fastq_number}_${sequence}"

    # write row
    echo "${sample_name},${fastq_id}"
  fi
done < fastq_files.txt | uniq > ${output_file_samples}

# Configure job genome-aligment
input_yaml="genome-alignment.yaml"
output_yaml="${user_name}_${input_yaml}"

echo -e "\nConfiguring job ${output_yaml}"

samples_data=$(cat ${output_file_samples} | sed 's/^/    /' )
awk -v samples="$samples_data" '{gsub("SAMPLES_CONFIGMAP", samples)}1' ${input_yaml} > ${output_yaml}
sed -i "s/SAMPLES_FILE_NAME/${output_file_samples}/g" ${output_yaml}

completions_alignment=$(wc -l < ${output_file_samples})
if [[ "${completions_alignment}" -le "${parallelism_alignment}" ]]; then
  parallelism_alignment=${completions_alignment}
fi
sed -i "s/username/${user_name}/g" ${output_yaml}
sed -i "s/PARALLELISM/${parallelism_alignment}/g" ${output_yaml}
sed -i "s/COMPLETIONS/${completions_alignment}/g" ${output_yaml}
sed -i "s/NFS_SERVER/${nfs_server}/g" ${output_yaml}
sed -i "s/NFS_PATH/${nfs_path}/g" ${output_yaml}
sed -i "s/NODE/${node}/g" ${output_yaml}

# Configure job merge-bams
input_yaml="merge-bams.yaml"
output_yaml="${user_name}_${input_yaml}"

echo -e "\nConfiguring job ${output_yaml}"

samples_data=$(cat ${output_file_samples} | sed 's/^/    /' )
awk -v samples="$samples_data" '{gsub("SAMPLES_CONFIGMAP", samples)}1' ${input_yaml} > ${output_yaml}

sed -i "s/SAMPLES_FILE_NAME/${output_file_samples}/g" ${output_yaml}
completions_merge=$(cut -d',' -f1 ${output_file_samples} | sort | uniq | wc -l)
if [[ "${completions_merge}" -le "${parallelism_merge}" ]]; then
  parallelism_merge=${completions_merge}
fi

sed -i "s/username/${user_name}/g" ${output_yaml}
sed -i "s/PARALLELISM/${parallelism_merge}/g" ${output_yaml}
sed -i "s/COMPLETIONS/${completions_merge}/g" ${output_yaml}
sed -i "s/NFS_SERVER/${nfs_server}/g" ${output_yaml}
sed -i "s/NFS_PATH/${nfs_path}/g" ${output_yaml}
sed -i "s/NODE/${node}/g" ${output_yaml}

# Create csv file with for mapping the bed files
echo -e "\nCreating ${output_bed_file_mapping} file"  
while IFS= read -r line; do
    # Extraer las dos primeras columnas de ./ref_genome/lc4.fa.fai y combinarlas con la línea actual de samples.csv
    tail -n +2 regions.txt | awk -v sample="$line" '{ print sample "," $1 ",0," $2 }'
done < <(cat ${output_file_samples} | cut -d ',' -f 1 | sort | uniq) > ${output_bed_file_mapping}

# Configure job variant call
input_yaml="genomic-variant-call.yaml"
output_yaml=${user_name}_genomic-variant-call.yaml
echo -e "\nConfiguring job ${output_yaml}"

beds_data=$(cat ${output_bed_file_mapping} | sed 's/^/    /' )
awk -v beds="$beds_data" '{gsub("MAPPING_CONFIGMAP", beds)}1' ${input_yaml} > ${output_yaml}
sed -i "s/MAPPING_FILE_NAME/${output_bed_file_mapping}/g" ${output_yaml}

completions_variant=$(cut -d',' -f1 ${output_bed_file_mapping} | wc -l)
if [[ "${completions_variant}" -le "${parallelism_variant}" ]]; then
  parallelism_variant=${completions_variant}
fi

sed -i "s/username/${user_name}/g" ${output_yaml}
sed -i "s/PARALLELISM/${parallelism_variant}/g" ${output_yaml}
sed -i "s/COMPLETIONS/${completions_variant}/g" ${output_yaml}
sed -i "s/NFS_SERVER/${nfs_server}/g" ${output_yaml}
sed -i "s/NFS_PATH/${nfs_path}/g" ${output_yaml}
sed -i "s/NODE/${node}/g" ${output_yaml}

echo -e "\n"

# Run workflow
if [ -z "$1" ]; then
    ./run_jobs.sh ${user_name}_genome-alignment.yaml ${user_name}_merge-bams.yaml ${user_name}_genomic-variant-call.yaml
elif [ "$1" = "-i" ]; then
    while true; do
        read -rp "Do you want to run the full workflow right now? (yes/no): " run_workflow
        if [[ "$run_workflow" == "yes" ]]; then
            # Run the workflow
	    ./run_jobs.sh ${user_name}_genome-alignment.yaml ${user_name}_merge-bams.yaml ${user_name}_genomic-variant-call.yaml
            break
        elif [[ "$run_workflow" == "no" ]]; then
            # Show alternative options
            echo -e "\nTo run the full workflow, launch this command:\n\n"
            echo "  ./run_jobs.sh ${user_name}_genome-alignment.yaml ${user_name}_merge-bams.yaml ${user_name}_genomic-variant-call.yaml"
            echo -e "\nAlternatively, you can manually launch the jobs using kubectl.yaml"
            break
        else
            echo "Invalid input. Please enter 'yes' or 'no'."
        fi
    done
fi

