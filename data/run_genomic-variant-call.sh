#!/bin/bash

# print start time
start_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Start Time: $start_time"

#MY_POD_NAME=$1
#MAPPING_FILE=$2
#INPUT_BED_FOLDER="data/beds"
#INPUT_BAM_FOLDER="data/bams"

# bed line to be retrieved
line_num=$(echo $MY_POD_NAME | grep -o '\-[0-9]\+\-' | tr -d '-' | awk '{print $1 + 1}')

# get sample name
sample_name=$(sed -n "${line_num}p" ${MAPPING_FILE} | cut -d ',' -f 1)
scaffold=$(sed -n "${line_num}p" ${MAPPING_FILE} | cut -d ',' -f 2)
start=$(sed -n "${line_num}p" ${MAPPING_FILE} | cut -d ',' -f 3)
end=$(sed -n "${line_num}p" ${MAPPING_FILE} | cut -d ',' -f 4)

# generate bed file
bed_file="${INPUT_BED_FOLDER}/${MY_POD_NAME}.bed"
#echo $(sed -n "${line_num}p" ${MAPPING_FILE} | cut -d ',' -f 2-) > ${bed_file}
echo $(sed -n "${line_num}p" ${MAPPING_FILE} | cut -d ',' -f 2- | tr ',' ' ') > ${bed_file}

echo $sample_name
cat "${INPUT_BED_FOLDER}/$MY_POD_NAME.bed"

# read variables from input text file
genome_name=$(echo "${REFERENCE_GENOME}" | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1)
input_bam="${INPUT_BAM_FOLDER}/${sample_name}_${genome_name}_ref.sorted.rg.merged_sorted.rmdup.indelrealigner.bam"
output_gvcf="${OUTPUT_GVCF_FOLDER}/${sample_name}_${genome_name}_ref_${scaffold}-${start}-${end}.g.vcf"

gatk HaplotypeCaller  \
   -R ${REFERENCE_GENOME} \
   -I ${input_bam} \
   -O ${output_gvcf} \
   -L ${bed_file} \
   --native-pair-hmm-threads ${THREADS} \
   -ERC GVCF

# print finish time
finish_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Finish Time: $finish_time"
