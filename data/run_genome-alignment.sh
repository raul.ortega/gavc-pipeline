#!/bin/bash

# Remember to prepare the reference genome in advance! Like this:
# ${BWA} index ${REFERENCE_GENOME}
# ${SAMTOOLS} faidx ${REFERENCE_GENOME}
# ${SAMTOOLS} dict ${REFERENCE_GENOME} -o ${REFERENCE_GENOME/.fa/.dict}

# print start time
start_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Start Time: $start_time"

# line to be retrieve from sample table
sample_line=$(echo $MY_POD_NAME | grep -o '\-[0-9]\+\-' | tr -d '-' | awk '{print $1 + 1}')

# get sample_name, fastq identifier and pairs r1 - r2 files
sample_name=$(sed -n "${sample_line}p" ${SAMPLES_FILE} | cut -d ',' -f 1)
fastq_id="$(sed -n "${sample_line}p" ${SAMPLES_FILE} | cut -d ',' -f 2)"
fastq_r1="${fastq_id}_R1.fastp.fastq.gz"
fastq_r2="${fastq_id}_R2.fastp.fastq.gz"

# Alignment
echo " - Aligning ${fastq_id} of ${sample_name} -"
${BWA} mem ${REFERENCE_GENOME} \
${IN_FASTQ_FOLDER}/${fastq_r1} \
${IN_FASTQ_FOLDER}/${fastq_r2} \
-t ${THREADS} | 
  ${SAMTOOLS} view -hbS -@ ${THREADS} - \
-o ${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.bam

# Sorting
echo " - Sorting ${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.bam -"
${SAMTOOLS} sort \
-@ ${THREADS} \
${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.bam \
-o ${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.sorted.bam

rm ${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.bam

# Add Read Groups
echo " - Adding Read Groups of ${sample_name}_${fastq_id}_${ALIGNMENT_NAME} -"
${PICARD} AddOrReplaceReadGroups \
I=${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.sorted.bam \
O=${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.sorted.rg.bam \
RGID=${fastq_id} RGLB=${sample_name}_lib \
RGPL=Illumina RGPU=${fastq_id} RGSM=${sample_name} \
VALIDATION_STRINGENCY=SILENT

rm ${OUT_BAM_FOLDER}/${sample_name}_${fastq_id}_${ALIGNMENT_NAME}.sorted.bam

echo "${sample_name} only has one r1-r2 fastq pair with id ${fastq_id}"
# Add merged_sorted to name to match alignments with multiple fastq r1-r2 pairs
mv ${out_bam_folder}/${sample_name}_${fastq_id}_${alignment_name}.sorted.rg.bam \
    ${out_bam_folder}/${sample_name}_${alignment_name}.sorted.rg.merged_sorted.bam


# print finish time
finish_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Finish Time: $finish_time"
