#!/bin/bash

# Remember to job genome_alignment.yaml before in order to generate the bams files to merge
# kubectl apply -f username_genome_alignmnet.yaml

# print start time
start_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Start Time: $start_time"

# line to be retrieve from sample table
sample_line=$(echo $MY_POD_NAME | grep -o '\-[0-9]\+\-' | tr -d '-' | awk '{print $1 + 1}')

# Leer el archivo CSV y obtener los valores distintos de la primera columna
sample_name=$(cut -d',' -f1 ${SAMPLES_FILE} | sort | uniq | sed -n "${sample_line}p")

fastq_id_values=$(awk -F',' -v val="$sample_name" '$1 == val { printf "%s ", $2 } END { printf "\n" }' ${SAMPLES_FILE} | xargs)

echo "- Processing sample $sample_name $fastq_id_values"

# convert to array
declare -a fastq_id
read -ra fastq_id <<< "$fastq_id_values"
    
if [ "${#fastq_id[@]}" -gt 1 ]; then

    echo "${sample_name} has multiple r1-r2 fastq pairs which need to be merged"
        
    # create list of bams to merge
    ls ${OUT_BAM_FOLDER}/${sample_name}_*_${ALIGNMENT_NAME}.sorted.rg.bam \
        > ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.bam.list
    
    # merge all bams of sample
    echo " - Merging ${sample_name}_${ALIGNMENT_NAME} bams"
    ${SAMTOOLS} merge -@ ${THREADS} \
	-r  -b ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.bam.list \
	${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged.bam

    for bam in $(cat ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.bam.list); do
	echo " - Removing ${bam} -"
	rm ${bam}
    done
    
    # sort the merged bam
    echo " - Sorting ${sample_name}_${ALIGNMENT_NAME} merged bam -"
    ${SAMTOOLS} sort  -@ ${THREADS} ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged.bam \
	-o ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.bam \
    
    rm ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged.bam
fi
    
# Mark Duplicated Reads
echo " - Mark Duplicated Reads of ${sample_name}_${ALIGNMENT_NAME} -"
${PICARD} MarkDuplicates \
    METRICS_FILE=${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.rmdup.txt \
    I=${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.bam \
    O=${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.bam \
    MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=800

rm ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.bam

# Indexing for GATK
echo " - Indexing ${sample_name}_${ALIGNMENT_NAME} -"
${SAMTOOLS} index ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.bam

# Create Target for Realignment:
echo " - RealignerTargetCreator on ${sample_name}_${fastq_id}_${ALIGNMENT_NAME} -"
${GATK} -T RealignerTargetCreator \
    -nt ${THREADS} -R ${REFERENCE_GENOME} \
    -I ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.bam \
    -o ${OUT_BAM_FOLDER}/${sample_name}_realignertargetcreator.intervals

# Realign INDELs 
echo " - IndelRealigner of ${sample_name}_${ALIGNMENT_NAME} -"
${GATK} -T IndelRealigner \
    -R ${REFERENCE_GENOME} \
    -targetIntervals ${OUT_BAM_FOLDER}/${sample_name}_realignertargetcreator.intervals \
    -I ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.bam \
    -o ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.indelrealigner.bam

rm ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.bam

# Index for downstream
echo " - Indexing ${sample_name}_${ALIGNMENT_NAME} final BAM for downstream analyses"
${SAMTOOLS} index ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.indelrealigner.bam

# Remove old indexes
rm ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.indelrealigner.bai
rm ${OUT_BAM_FOLDER}/${sample_name}_${ALIGNMENT_NAME}.sorted.rg.merged_sorted.rmdup.bam.bai

# print finish time
finish_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Finish Time: $finish_time"
