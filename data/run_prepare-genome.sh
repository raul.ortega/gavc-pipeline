#!/bin/bash

# print start time
start_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Start Time: $start_time"

${BWA} index ${REFERENCE_GENOME}
${SAMTOOLS} faidx ${REFERENCE_GENOME}
${SAMTOOLS} dict ${REFERENCE_GENOME} -o ${REFERENCE_GENOME/.fa/.dict}

# print finish time
finish_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Finish Time: $finish_time"
